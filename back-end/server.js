const express = require('express');
const app = express();
const cors = require('cors');
// init middleware
app.use(express.json());
app.use(cors());
//app.use(express.urlencoded({extended:true}));

// employee route
app.use('/api/employee', require('./server/routes/employeeRoute'));


const PORT = process.env.PORT || 8080;

app.listen(PORT, ()=>console.log(`Server started on port ${PORT}`));