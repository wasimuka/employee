module.exports = (sequelize, Sequelize) => {
  const modelName = "employee";
  const attributes = {
    firstName: {type: Sequelize.STRING},
    lastName: {type: Sequelize.STRING},
    email: {type: Sequelize.STRING},
    designation: {type: Sequelize.INTEGER},
    inTime: {type: Sequelize.TIME},
    outTime: {type: Sequelize.TIME},
    inNote: {type: Sequelize.TEXT},
    outNote: {type: Sequelize.TEXT},
  };
  const options = {
    //prevent sequelize from pluralizing table names
    //freezeTableName: true,
    timestamps: false
  };
  //create table quiz
  const employee = sequelize.define(modelName, attributes, options);

  return employee;
};
