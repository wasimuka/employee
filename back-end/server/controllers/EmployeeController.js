const employeeService = require('../services/EmployeeService');
const {validationResult} = require('express-validator');
class Employee{

  //get question list
  async index(req, res){
    let isSuccess = await employeeService.index(req.query);
    return Employee.sendResponse(res, isSuccess);
    
  }

  //get question object
  async get(req, res){
    let isSuccess = await employeeService.get(req.params.id);
    return Employee.sendResponse(res, isSuccess);
  }

  // update or store a question
  async store(req, res){
    let errors = null, isSuccess = null;
    //validate the request.
    errors = validationResult(req);
    if(!errors.isEmpty()){
      res.status(422).json({error: errors.array()});
    }
    //update or store the question
    if(req.params.id){
      isSuccess = await employeeService.update(req.params.id, req.body);
    }else{
      isSuccess = await employeeService.store(req.body);
    }
    return Employee.sendResponse(res, isSuccess);

  }
  //delete a question
  async destroy(req, res){
    let ids = req.params.id ? [req.params.id] : req.query.ids.split(',');
    let isSuccess = await employeeService.destroy(ids);
    return Employee.sendResponse(res, isSuccess, 0);
  }

  /**
   * @param object res
   * @param  boolean isSuccess
   * @param int requestType [get->1, post->1, delete->0] 
   */
  static sendResponse(res, isSuccess, requestType=1){
    if(requestType && isSuccess){
      //handel get | post
      return res.json(employeeService.getResult());
    }else if(!requestType && isSuccess){
      //handel delete
      return res.status(204).send();
    }
    //handel error
    let errors = employeeService.getError();
    return res.status(errors.code).json({error: errors.error});
  }
}

module.exports = new Employee();
