const router = require('express').Router();
const employeeCtrl = require('../controllers/EmployeeController');
const {check} = require('express-validator');
const validationRule = [
    check('firstName', 'Please enter First Name')
];

// @route   GET /
// @desc    Return Question List
// @Access  Public
router.get('/', employeeCtrl.index);
// @route   GET /
// @desc    Return a question object
// @Access  Public
router.get('/:id', employeeCtrl.get);

// @route   POST /
// @desc    STORE or UPDATE a question
// @Access  Private
router.post('/', validationRule, employeeCtrl.store);
router.post('/:id', employeeCtrl.store);

// @route   DELETE /
// @desc    Delete question(s)
// @Access  Private
router.delete('/', employeeCtrl.destroy);

module.exports = router;