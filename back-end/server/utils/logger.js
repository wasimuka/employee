//require log4js
const log4js = require('log4js');
//logger configuration
log4js.configure({
appenders: {fileAppender:{type:'file',filename:'./logs/allLogs.log'}},
categories:{default:{appenders:['fileAppender'],level:'debug'}}
 
});
// create the logger
const logger = log4js.getLogger();

module.exports = logger;
