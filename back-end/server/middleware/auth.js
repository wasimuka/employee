const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");

module.exports = function(req, res, next){
  const token = req.header('token');

  //check token
  if(!token){
    return res.status(401).json({error: {msg:'No token, authorization denied'}});
  }

  // validate token
  try{
    const decode = jwt.verify(token, config.secret);
    req.user = decode.user;
    next();
  }catch(err){
    console.log('Middleware error: ', err);
    return res.status(401).json({error:{msg: 'Invalid token, authorization denied'}});
  }
}
