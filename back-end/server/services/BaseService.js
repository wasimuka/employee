const Op = require('sequelize').Op;
class BaseService{
    constructor(){
        this._data = null;
        this._error = {msg:{msg:'Internal server error'}, code:500};
    }

    /**
     * 
     * @returns error object
     */ 
    getError(){
        return this._error;
    }

    /**
     * 
     * @param err error object 
     * @param code http error code
     */
    setError(err, code){
        this._error.msg = err;
        this._error.code = code;
    }

    /**
     * @returns current object
     */
    getResult(){
        return this._data;
    } 

    /**
     * 
     * @param data store current result 
     */
    setResult(data){
        this._data = data;
    }

    /**
     * @param object query parameters
     * @returns object of where condition
     */
    buildFilter(query){
        let options = {};
        options.where = {};
        options.order = [['id', 'desc']];

        if(query.select){
            options.attributes = query.select.split(',');
        }
        
        if(query.offset){
            options.offset = query.offset;
        }

        if(query.limit){
            options.offset = query.limit;
        }

        if(query.search){
            if(isNaN(query.search)){
                options.where.title = {[Op.like]:`%${query.search}%`};
            }else{
                options.where.id = +query.search;
            }
        }

        if(query.type){
            options.where.type = query.type;
        }

        if(query.except){
            let ids = query.except.split(',');
            options.where.id = {[Op.notIn]: ids};
        }

        return options;

    }


}

module.exports = BaseService;