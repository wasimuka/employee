const db = require("../models");
const Op = db.Sequelize.Op;
const BaseService = require('./BaseService');

class QuestionService extends BaseService{

    /**
     * @param object optional parameter
     * @returns boolean 
     */
    async index(query){
        try{
            // apply filters
            let options = this.buildFilter(query);
            let res = await db.Employee.findAll(options);
            this.setResult(res);
            return true;
        }catch(err){
            console.log(err);
            return false;
        }
        
    }

    /**
     * @param INT id
     * @returns boolean 
     */
    async get(id){
        try {
            let employee = await db.Employee.findByPk(id, {
                include:{
                    model: db.Option
                }
            });
            this.setResult(employee);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
        
    }
    /**
     * @param object Employee and its options
     * @returns boolen 
     */
    async store(data){
        try{
            let employee = await db.Employee.create(data);
            this.setResult(employee);
            return true;
        }catch(err){
            console.log(err);
            return false;
        }
    }

    /**
     * @param INT Employee id
     * @param objct Employee and its option
     * @returns boolean
     */
    async update(id, data){
        try {
            //find question
            let employee = await db.Employee.findByPk(id);
            if(!employee){
                this.setError({msg:'Not a valid employee id'}, 404);
                return false;
            }
            //update Employee.
            employee = await db.Employee.update(data, { where: {id}});
            this.setResult(employee);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
        
    }
    /**
     * @param Array question ids
     * @return boolean 
     */
    async destroy(ids){
        try {
            await db.Employee.destroy({where: {id: {[Op.in]: ids}}});
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }

    }

} 

module.exports = new QuestionService();