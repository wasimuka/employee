import React from 'react';
import { Route, Switch } from 'react-router-dom';
import List from './views/List';
const Employee = (props) => {
    const type = props.type;
    return (
        <Switch>
            <Route exact path={props.match.path} render={(props)=> <List type={type} {...props} />} />
        </Switch>
    );
}
export default Employee;