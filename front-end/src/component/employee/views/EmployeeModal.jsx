import React, { useState } from 'react'
import { Modal } from 'react-bootstrap';
import Form from '../../../themes/default/Form/Form';
import Label from '../../../themes/default/Label/Label';
import TextField from '../../../themes/default/Inputs/Inputs';
import SelectList from '../../../themes/default/SelectList/SelectList';
import { Exit, Save } from '../../../themes/default/Buttons/MultiButtons';
import { postReq } from '../../../http/api';
import FormValidation from '../../../utils/FormValidation';
const EmployeeModal = props => {
    const [state, update] = useState({
        firstName: '',
        lastName: '',
        designation: 0,
        email: ''
    });

    const handleChange = (field, value) => {
        update({
            ...state,
            [field]: value
        });
    };

    const saveEmployee = async () =>{
        const data = state;
        for(const [key, value] of Object.entries(data)){
            let result = FormValidation.check(key, value);
            if(result.flag){
                alert(result.msg);
                return false;
            }
        }
        const result = await postReq('/employee', data);
        props.onHide(null,true);
    }


    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Add Employee
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    {/* Left side div  */}
                    <div className='col-sm-12'>


                        <div className='form-group col-sm-12 text-left'>
                            <h6>First Name</h6>
                            <TextField
                                className={['form-control', 'col-sm-9', 'd-inline-block']}
                                placeholder='First Name'
                                onChange={(e)=>handleChange('firstName', e.target.value)}
                                value={`${state.firstName}`}
                            />
                        </div>
                        <div className='form-group col-sm-12 text-left'>
                            <h6>Last Name</h6>
                            <TextField
                                className={['form-control', 'col-sm-9', 'd-inline-block']}
                                placeholder='Last Name'
                                onChange={(e)=>handleChange('lastName', e.target.value)}
                                value={`${state.lastName}`}
                            />
                        </div>

                        <div className='form-group  col-sm-12 text-left'>
                            <h6>Select Designation</h6>
                            <SelectList
                                className={['form-control', 'col-sm-9', 'd-inline-block']}
                                id='type'
                                list={[{id:0, title:'Software Engineer'}, {id:1, title:'Manger'},{id:2, title:'Sr. Developer'}, {id:3, title:'Designer'}]}
                                onChange={(e)=>handleChange('designation', e.target.value)}
                                value={state.designation}
                            />
                        </div>


                        <div className='form-group col-sm-12 text-left'>
                            <h6>Email Id</h6>
                            <TextField
                                className={['form-control', 'col-sm-9', 'd-inline-block']}
                                placeholder='Email Id'
                                onChange={(e)=>handleChange('email', e.target.value)}
                                value={`${state.email}`}
                            />
                        </div>
                    </div>


                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Save onClick={saveEmployee} />
                <Exit onClick={props.store} />
            </Modal.Footer>
        </Modal>

    );
};

export default EmployeeModal;