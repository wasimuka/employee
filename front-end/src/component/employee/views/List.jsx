import React, { Component } from 'react'
import HeaderWrapper from '../../../themes/default/HeaderWrapper';
import { Add } from '../../../themes/default/Buttons/MultiButtons';
import List from '../../../themes/default/ViewWrappers/List';
import { TableCell, TableBody, TableRow, TableHeading, TableHead } from '../../../themes/default/Table/Table';
import { Table } from 'react-bootstrap';
import { getReq } from '../../../../http/api';
import TimeModal from './TimeModal';
import EmployeeModal from './EmployeeModal';
class employeeList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // employees: [
            //     { id: 1, name: 'Wasi', designation: 'Engineer', inTime: 5, outTime: 6 },
            //     { id: 2, name: 'Hamza', designation: 'Engineer', inTime: 5, outTime: 6 },
            // ],
            employees: [],
            showAddEmployee: false,
            showInTime: false,
            showOutTime: false,
            designation: ['Software Engineer', 'Manger', 'Sr. Developer', 'Designer']
        }
    }

    showInTime = (e, reload = false) => {
        this.setState({ showInTime: !this.state.showInTime });
        if (reload) {
            this.getAll();
        }
    }

    showOutTime = (e, reload = false) => {
        this.setState({ showOutTime: !this.state.showOutTime });
        if (reload) {
            this.getAll();
        }
    }

    showAddEmployee = (e, reload = false) => {
        this.setState({ showAddEmployee: !this.state.showAddEmployee });
        console.log(reload);
        if (reload) {
            this.getAll();
        }
    }


    async getAll() {

        let result = await getReq('/employee');
        this.setState({ employees: result });

    }
    componentDidMount() {
        this.getAll();
    }


    render() {
        const employees = this.state.employees;
        return (
            <List>
                <HeaderWrapper title={'Employees'}>
                    <Add title="Add Employee" onClick={this.showAddEmployee} />
                    <Add title="In Time" onClick={this.showInTime} />
                    <Add title="Out Time" onClick={this.showOutTime} />
                </HeaderWrapper>
                <Table className={["employee-table", "table", "table-striped"]}>
                    <TableHead>
                        <TableRow>

                            <TableHeading >
                                Employee Name
                            </TableHeading>
                            <TableHeading >
                                Designation
                            </TableHeading>
                            <TableHeading >
                                In Time
                            </TableHeading>
                            <TableHeading  >
                                Out Time
                            </TableHeading>
                            <TableHeading >
                                Duration (outime - intime) hours
                            </TableHeading>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Array.isArray(employees) && employees.map((employee, index) => {
                            let hours = 'NA';
                            if (employee.inTime && employee.outTime) {
                                let arr = [employee.outTime, employee.inTime].map(function (time) {
                                    time = time.split(':')
                                    let hh = parseInt(time[0], 10);
                                    let mm = parseInt(time[1], 10);
                                    return (hh * 60 + mm) / 60;
                                });
                                hours =  `${(arr[0] - arr[1]).toFixed(1)} hours`;
                            }
                            return (
                                <TableRow key={index}>
                                    <TableCell>
                                        {`${employee.firstName} ${employee.lastName}`}
                                    </TableCell>
                                    <TableCell>
                                        {this.state.designation[employee.designation]}
                                    </TableCell>
                                    <TableCell>
                                        {employee.inTime ? employee.inTime : 'Add'}
                                    </TableCell>
                                    <TableCell>
                                        {employee.outTime ? employee.outTime : 'Add'}
                                    </TableCell>
                                    <TableCell>
                                        {hours}
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>

                <TimeModal
                    show={this.state.showInTime}
                    onHide={this.showInTime}
                    title="Employee Check In Time"
                    type="IN"
                    employees={this.state.employees}
                />
                <TimeModal
                    show={this.state.showOutTime}
                    onHide={this.showOutTime}
                    title="Employee Check Out Time"
                    type="OUT"
                    employees={this.state.employees}
                />

                <EmployeeModal
                    show={this.state.showAddEmployee}
                    onHide={this.showAddEmployee}
                />
            </List >
        );
    }
}

export default employeeList;

