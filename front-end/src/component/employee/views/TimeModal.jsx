import React, { useState } from 'react'
import { Modal } from 'react-bootstrap';
import Form from '../../../themes/default/Form/Form';
import TextField from '../../../themes/default/Inputs/Inputs';
import SelectList from '../../../themes/default/SelectList/SelectList';
import { Exit, Save } from '../../../themes/default/Buttons/MultiButtons';
import { hour, minute } from './data';
import { postReq } from '../../../http/api';
const TimeModal = props => {
    const [state, update] = useState({
        empId: null,
        hour: null,
        minute: null,
        note: '',
        type: props.type
    });

    const handleChange = (field, value) => {
        update({
            ...state,
            [field]: value
        });
    };

    const updateTime = async ()=>{
        let data = {};
        if(state.type == 'IN'){
            data.inTime = `${state.hour}:${state.minute}`;
            data.inNote = state.note;
        }else{
            data.outTime = `${state.hour}:${state.minute}`;
            data.outNote = state.note;
        }
        const result  = await postReq(`/employee/${state.empId}`, data);
        result && props.onHide(null,true);
        
    }
    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {props.title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    {/* Left side div  */}
                    <div className='col-sm-12'>

                        <div className='form-group  col-sm-12 text-left'>
                            <h6>Select employee</h6>
                            <SelectList
                                className={['form-control', 'col-sm-9', 'd-inline-block']}
                                list={props.employees}
                                onChange={(e) => handleChange('empId', e.target.value)}
                                value={state.empId}
                            />
                        </div>


                        <div className='form-group col-sm-12 text-left'>
                            <h6>Hour</h6>
                            <SelectList
                                className={['form-control', 'col-sm-9', 'd-inline-block']}
                                list={hour}
                                onChange={(e) => handleChange('hour', e.target.value)}
                                value={state.hour}
                            />
                        </div>

                        <div className='form-group col-sm-12 text-left'>
                            <h6>Minute</h6>
                            <SelectList
                                className={['form-control', 'col-sm-9', 'd-inline-block']}
                                list={minute}
                                onChange={(e) => handleChange('minute', e.target.value)}
                                value={state.minute}
                            />
                        </div>


                        <div className='form-group col-sm-12 text-left'>
                            <h6>Note</h6>
                            <TextField
                                className={['form-control', 'col-sm-9', 'd-inline-block']}
                                placeholder='Note'
                                onChange={(e) => handleChange('note', e.target.value)}
                                value={`${state.note}`}
                            />
                        </div>
                    </div>


                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Save onClick={updateTime} />
                <Exit onClick={props.onHide} />
            </Modal.Footer>
        </Modal>

    );
};

export default TimeModal;