import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import {Provider} from 'react-redux';
import './index.css';
import App from './App';
import store from './store';

let html = (
    <div className="App">
        <Provider store = {store} >
            <BrowserRouter>
            <App/>
            </BrowserRouter>
        </Provider>
    </div>
)

ReactDOM.render(html, document.getElementById('root'));
