import axios from './Axios';

export const postReq = async (url, data={}, id=0)=>{
    try {
        if(id){
            url = `${url}/${id}`;
        }
        let res = await axios.post(url, data);
        return res.data;
    } catch (err) {
        console.log(err);
        return false;
    }
};

export const getReq = async (url, params = {})=>{
    try {
        let res = await axios.get(url, {params});
        return res.data;
    } catch (err) {
        console.log(err);
        return false;
    }
};

export const deleteReq = async (url , params={})=>{
    try {
        let res = await axios.delete(url, {params});
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}