class FormValidation {
  constructor() {
    this.message = {
      name: "Please enter name",
      email: "Enter a valid email id",
      userName: 'Please enter user name',
      password: 'password can\'t be empty',
      firstName: 'Please enter first name',
      lastName: 'Please enter last name',
    };

  }

  /**
   * @param  key field name
   * @param  value field value
   * @return object->{flag = true||false, msg }
   */
  check(key, value) {
    // console.log("adssa", value);
    let flag = false;
    let regExp = null;
    switch (key) {
      case "id":
        flag = false;
        break;
      case "userName":
        flag = value !== "" ? false : true;
        break;
      case "name":
        flag = value !== "" ? false : true;
        break;
      case "firstName":
        flag = value !== "" ? false : true;
        break;
      case "lastName":
        flag = value !== "" ? false : true;
        break;

      case "mob_no":
        regExp = /^\d{10}$/;
        flag = regExp.test(value) ? false : true;
        break;
      case "email":
        regExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        flag = regExp.test(value) ? false : true;
        break;

      default:
        flag = false;
    }

    const obj = { flag };
    if (flag) obj.msg = this.message[key];
    return obj;
  }
}

export default new FormValidation();
