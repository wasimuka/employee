import React from "react";

import ReactDatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


const DatePicker = ({ id, selected, onChange, className, ...others }) => {
  return (
    <ReactDatePicker
      id={id}
      className={Array.isArray(className) ? className.join(' ') : ''}
      selected={ new Date(selected) || new Date()}
      onChange={onChange}
      showYearDropdown
      dateFormatCalendar="MMMM"
      yearDropdownItemNumber={15}
      scrollableYearDropdown
      showMonthDropdown
      useShortMonthInDropdown
      {...others}
      dateFormat = 'dd-MM-yyyy'
    />
  )
}

export default DatePicker;
