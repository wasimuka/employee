import React from "react";

import ReactDatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


const TimePicker = ({ id, selected, onChange, className, ...others }) => {
  const ExampleCustomInput = ({ value, onClick }) => (
    <input className={className} type='text' value={value} onClick={onClick} />
  );
  return (
    <ReactDatePicker
      id={id}
      className={Array.isArray(className) ? className.join(' ') : ''}
      selected={ selected && new Date(selected) || new Date()}
      onChange={onChange}
      showTimeSelect
      showTimeSelectOnly
      timeIntervals={15}
      timeCaption="Time"
      dateFormat="hh"
      customInput={<ExampleCustomInput className={className} />}
    />
  )
}

export default TimePicker;
