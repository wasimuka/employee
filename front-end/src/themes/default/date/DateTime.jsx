import React from "react";

const DateTime = props => {
  return(
    <input
      className={Array.isArray(props.className)?props.className.join(' '):''}
      type="datetime-local"
      name={props.name}
      id={props.id}
      value={props.value}
      onChange={props.onChange}
      min={props.min}
      max={props.max}
    />
  );
}

export default DateTime;
