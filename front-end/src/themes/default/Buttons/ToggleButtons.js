import React from 'react';
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';

const ToggleButtons = props =>{
    return (
        <ToggleButtonGroup  
            type={props.type ? props.type : 'radio'} 
            name= {props.name ? props.name : 'toggleButton'}
            defaultValue={props.value} 
            onChange={props.onChange}
            id={props.id}
        >
            <ToggleButton value={1} variant={props.value ? 'success': 'secondary'}>{props.title[0]}</ToggleButton>
            <ToggleButton value={0} variant={!props.value ? 'danger': 'secondary'}>{props.title[1]}</ToggleButton>
                        
        </ToggleButtonGroup>
    );
}

export default ToggleButtons;