import React from 'react';


const Widget = ({className,children,...rest}) => {
    return (
        <div className={`widget ${Array.isArray(className)?className.join(' '):''}`}
        {...rest}
        >
            {children}
        </div>
    );
}

export default Widget;
