import React from 'react';

const nav = (props) => {
    return (
        <nav className={Array.isArray(props.className)?props.className.join(' '):''} id={props.id}>{props.children}
		</nav>        
    );
}


export default nav;