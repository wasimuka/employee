import React from 'react'
import PropTypes from 'prop-types'

const ComponentError = (props) => {
    let { message, component } = props;
    let messageText = props.message || `Unable to load the ${component}`
    return (
        <div className={"component-error"}>{messageText}</div>
    )
}

ComponentError.protoTypes = {
    message: PropTypes.string,
    component: PropTypes.string
}

export default ComponentError;