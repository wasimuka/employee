import React from "react"
const List = (props) => {
    return (
        <>
            <div className="tablelist col-sm-12">
                <div className="tablelist_inner">
                    {props.children}
                </div>
            </div>
        </>
    )
}

export default List;
