import React from "react"

const Form = (props) => {
    return (
        <>
            <div className="uform col-sm-12">
                <div className="uform_inner user_form">
                    {props.children}
                </div>
            </div>
        </>
    )
}

export default Form;