import React from "react"

const Message = ({success,message}) => {
    return (
        <div className="form-group col-sm-12 messages">
            <div
                className={
                    success
                        ? "alert alert-success"
                        : "alert alert-danger"
                }
            >
                <span>{message}</span>
            </div>
        </div>
    )
}

export default Message;