import React from 'react'

const HorizontalScroll = (props) => {
    return <div className={["horizontal-scroll"]}>{props.childrens}</div>
}
const VerticalScroll = (props) => {
    return <div className={["vertical-scroll"]} > { props.childrens }</ div>
        }
export {HorizontalScroll, VerticalScroll}