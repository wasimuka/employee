import React from "react";

const Form = props => {
  return (
    <>
      <form
        className={
          Array.isArray(props.className) ? props.className.join(" ") : ""
        }
        onSubmit={props.onSubmit}
        name={props.name}
        id={props.id}
        method={props.method}
        encType={props.encType}
      >
        <>
          {props.children}
        </>
      </form>
    </>
  );
};

export default Form;
