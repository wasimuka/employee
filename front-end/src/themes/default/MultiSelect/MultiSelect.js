import React from "react";
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';
import PropTypes from 'prop-types';
// ...

const MultiSelect = ({options,...rest}) => {
  return (
    <ReactMultiSelectCheckboxes options={options} {...rest} />
  )
}

MultiSelect.propTypes = {
  options: PropTypes.array,
  onChange: PropTypes.func.isRequired,
}

export default MultiSelect;
