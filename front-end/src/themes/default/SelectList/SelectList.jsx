import React from 'react';

const SelectList = (props) => {

  let option = <option value="" >No data found</option>;
  if (Array.isArray(props.list) && props.list.length !== 0) {
    option = props.list.map((key, index) => {
      if(typeof key =='object'){
        return <option value={key.id} key={key.id}>{key.name ? key.name : key.firstName ? `${key.firstName} ${key.lastName}` : key.title }</option>
      }else{
        return <option value={key} key={index}>{key}</option>
      }
    });
  }

  let optionSelectOne = null;
  if (!props.hideDefaultOption)
    optionSelectOne = <option disabled={(props.disableOption) ? "disabled" : false} value={props.default || ""} >{"Select one..."}</option>;

  const demo = () => { }

  return (
    <select
      disabled={props.disabled ? "disabled" : false}
      className={Array.isArray(props.className) ? props.className.join(' ') : ''}
      id={props.id}
      value={props.value || 0}
      onChange={props.onChange || demo}
      onClick={props.onClick || demo}
      multiple={props.multiple ? "multiple" : null}
    >
      {optionSelectOne}
      {option}
    </select>
  );
}

export default SelectList;
