import React from 'react';

const Radio = (props) => {
    return (
        <input 
            className={Array.isArray(props.className)?props.className.join(' '):''} 
            onChange={props.onChange} 
            type="radio" 
            name={props.name} 
            id={props.id} 
            value={props.value} 
            checked={props.checked}
        />        
    );
};

const TextField = props => {
    return (
      <input
        className={
          Array.isArray(props.className) ? props.className.join(" ") : ""
        }
        onChange={props.onChange}
        // {...rest}
        onClick={props.onClick}
        onBlur={props.onBlur}
        type={props.type || "text"}
        name={props.name}
        id={props.id}
        required={props.required}
        pattern={props.pattern}
        title={props.title}
        placeholder={props.placeholder}
        value={props.value || ''}
        autoComplete={props.autoComplete}
        readOnly={props.readOnly || false}
        disabled={props.disabled || false}
      />
    );
};

const TextArea = (props) => {
    return (
        <textarea 
            className={Array.isArray(props.className)?props.className.join(' '):''} 
            onChange={props.onChange}  
            name={props.name} 
            id={props.id} 
            placeholder={props.placeholder} 
            value={props.value} 
            rows={props.rows} 
            cols={props.cols}
        />        
    );
};

const CheckBox = props => {
    return (
      <input
        className={ Array.isArray(props.className) ? props.className.join(" ") : "" }
        onChange={props.onClick}
        type="checkbox"
        name={props.name}
        id={props.id}
        data-index={props.index || 0}
        value={props.value}
        checked={props.checked}
        style={props.style}
      />
    );
};

const File = (props) => {
    return props.multiple === true ? (
        <input 
            ref={props.fileRef}  
            className={Array.isArray(props.className) ? props.className.join(' ') : ''} 
            onChange={props.onChange} 
            type="file" name={props.name} 
            id={props.id} placeholder={props.placeholder} 
            value={props.value} 
            accept={props.accept} 
            multiple 
        />
    ) :
    (
        <input 
            ref={props.fileRef}  
            className={Array.isArray(props.className) ? props.className.join(' ') : ''}
            onChange={props.onChange} 
            type="file" 
            name={props.name}
            id={props.id}
        />
    );
}


export {Radio, TextField, CheckBox, TextArea, File};

