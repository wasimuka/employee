import React from 'react';

const Radio = (props) => {
    return (
        <input className={Array.isArray(props.className)?props.className.join(' '):''} onChange={props.onChange} type="radio" name={props.name} id={props.id} value={props.value} checked={props.checked}/>        
    );
}


export default Radio;

