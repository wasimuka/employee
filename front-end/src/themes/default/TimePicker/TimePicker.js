import React, { useState } from "react";

import RCTimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';


const TimePicker = ({ id,value, onChange, className, placeholder,...others }) => {
  return(
    <RCTimePicker
    id={id}
    className ={Array.isArray(className)?className.join(' '):''}
    placeholder={placeholder || "HH:MM:SS"}
    value={value}
    onChange={onChange}
    {...others}
    />

  )
}

// const TimePicker = ({ id, value, onChange, className, placeholder, ...others }) => {
//   //let [clicked,setClick] = useState(false);
//   return (
//     <div>
//       <label for={placeholder.toLowerCase()}>{placeholder}</label>
//       <input type={"time"} onChange={onChange} className={className}
//         value={value} placeholder={placeholder} />
//     </div>
//   )
// }

export default TimePicker;
