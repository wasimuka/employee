import React from "react";
import Icon from "./Icon";

let debug = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const FileIcon = props => {
    let { extension, className } = props;
    if (!extension || extension === "") return "";
    if (typeof extension !== "string") {
        if (debug) console.log("Invalid extension passed on");
        return "";
    }

    let icon = null;
    switch (extension.toLowerCase()) {
        case 'jpg':
        case 'jpeg':
        case 'png':
            icon = "faFileImage";
            break;
        case 'pdf':
            icon = "faFilePdf";
            break;
        case 'docs':
        case 'docx':
        case 'doc':
            icon = "faFileWord";
            break;
        default:
            icon = null;
    }
    if (!icon) return "";
    // console.log("calculated icon",icon);

    return (
        <Icon
            className={
                Array.isArray(className) ? className.join(" ") : ""
            }
            icon={icon}
        >
            {props.children}
        </Icon>
    );
};

export default FileIcon;
