import React from 'react';
import PropTypes from 'prop-types'
import { Link } from "react-router-dom";

const GridView = (props) => {
    let { tiles = [], className = [], tileClassName = [], cols, link } = props
    if (typeof className === 'string') className = className === '' ? [] : className.split(" ")
    if (typeof tileClassName === 'string') tileClassName = tileClassName === '' ? [] : tileClassName.split(" ")
    if (tiles.cols > 0) {
        className.push('col-lg-' + tiles.cols);
    }
    className.push("t-grid-tiles");
    
    let linkProps = null;
    return (
        <div className={"t-grid-view"}>
            <div className={[...className].join(" ")}>
                {tiles.map((item, index) => {
                    if (item.link) {
                        linkProps = item.link;
                    } else {
                        linkProps = { to: "#" }
                    }
                    console.log("linkProps", linkProps);
                    return (
                        <Link {...linkProps}>
                            <div key={index} className={["tile", ...tileClassName].join(" ")}>
                                {item.icon}
                                <span>{item.title}</span>
                            </div>
                        </Link>
                    )
                })}
            </div>
        </div>
    )

}

GridView.propTypes = {
    tiles: PropTypes.arrayOf(
        PropTypes.shape({
            cols: PropTypes.number,
            title: PropTypes.string,
        })),
    className: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    tileClassName: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    link: PropTypes.shape({
        to: PropTypes.string,
        value: PropTypes.string,
        onClick: PropTypes.func
    })
}

export default GridView;