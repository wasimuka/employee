import React from 'react';

const Select = (props) => {
    return (
        <select {...props}>        
            {props.children}
        </select>
    );
}

const Option = (props) => {
    return (
        <option {...props}>
            {props.children}
        </option>
    );
}


export default Select;
export {Option};
