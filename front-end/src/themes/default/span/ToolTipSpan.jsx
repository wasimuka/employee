import React from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
const ToolTipSpan = props =>{

    return (
        <>
            <OverlayTrigger
                key={props.children}
                placement={props.placement || 'top'}
                overlay={
                    <Tooltip id={`tooltip-${props.placement || 'top'}`}>
                        <strong>{props.tip || 'Desc'}</strong>.
                    </Tooltip>
                }
            >
                <span
                    className={Array.isArray(props.className) ? props.className.join(' ') : ''}
                    name={props.name} id={props.id}
                    onClick={props.onClick}
                >
                    {props.children}
                </span>
            </OverlayTrigger>
        </>
    );
}

export default ToolTipSpan;