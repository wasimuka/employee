import {showMessage} from '../../default/MessageBoard';
const HandleResponseError = (err, that) =>{
    if(err.hasOwnProperty('response')){
        let errRep = err.response;
        if(errRep.status === 422){
            for( let key in errRep.data.detail){
                console.log(errRep.data.detail[key].join('\n'));
                showMessage({title:'invalid field', message:errRep.data.detail[key].join('\n')}, that.context);
            }
        }else if(errRep.status === 500){
            showMessage({title:errRep.data.title, message:errRep.data.detail}, that.context);
        }
    }
}

export default HandleResponseError;