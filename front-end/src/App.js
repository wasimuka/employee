import React, {useEffect} from 'react';
import {Route, Switch} from 'react-router-dom';
import Admin from './components/admin';
import Site from './components/site';
import AuthState from './context/auth/AuthState';
import {setToken} from './http/Axios';
setToken(localStorage.getItem('token'));
const App = props => {

  return (
    <AuthState>
    <Switch>
      <Route path='/admin' component={Admin} />
      <Route path='/' component={Site} />
    </Switch>
    </AuthState>
  );
}


export default App;
